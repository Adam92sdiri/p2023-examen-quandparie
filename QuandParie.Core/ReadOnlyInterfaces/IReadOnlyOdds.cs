﻿using System;

namespace QuandParie.Core.ReadOnlyInterfaces
{
    public interface IReadOnlyOdds
    {
        bool CanBeWagered { get; }
        string Condition { get; }
        Guid Id { get; }
        string Match { get; }
        bool? Outcome { get; }
        decimal Value { get; }
    }
}