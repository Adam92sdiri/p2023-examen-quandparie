﻿using QuandParie.Core.Domain;
using System;
using System.Threading.Tasks;

namespace QuandParie.Core.Persistance
{
    public interface IDocumentRepository
    {
        Task SaveAsync(DocumentType documentKind, string documentId, byte[] document);
    }
}